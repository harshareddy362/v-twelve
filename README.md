## V-Twelve

___A web application that allows users to create and attempt the polls based on their respective need.
Each poll can be attemped by only 12 registered users.
User can't attempt/create poll without Login.
User can access the desired poll by using the link of respective poll___
---
### About project directories
* The name of the project is the website. 
* Files like settings.py e.t.c of the project are found in the website directory.
* The poll is an application present in a project called the website.

---
#### Commands used to create project on local system
```python
$ django-admin startproject website
website:~$ django-admin startapp poll
```
#### Commands to run the server
* Declare the respective database host in settings.py
* Then follow the following commands to run the server
```python
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
```
**The surver will be running on the local host, Then navigate to /home**

---

Programming language | Framework | Database
------------ | ------------- |------------
Python | Django 3.1 | Mysql
