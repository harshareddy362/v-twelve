# imported required modules

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import PollCreation


# Class that generates the form for user registration

class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


# Class that generates the form for the poll creation

class CreatePollForm(forms.ModelForm):
    title = forms.CharField(widget=(forms.Textarea(attrs={
        'rows': 5,
        'class': 'form-inline',
        'style': 'resize:none;',
    }
    )))

    class Meta:
        model = PollCreation
        fields = ['title', 'option1', 'option2', 'option3', 'option4', 'option5']
