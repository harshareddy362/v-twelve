# imported required modules

from django.contrib.auth.models import User
from django.db import models


# models are here.


# model class to create and save the poll data

class PollCreation(models.Model):
    title = models.TextField(max_length=1000)
    option1 = models.CharField(max_length=500)
    option2 = models.CharField(max_length=500)
    option3 = models.CharField(max_length=500)
    option4 = models.CharField(max_length=500)
    option5 = models.CharField(max_length=500)
    c1 = models.IntegerField(default=0)
    c2 = models.IntegerField(default=0)
    c3 = models.IntegerField(default=0)
    c4 = models.IntegerField(default=0)
    c5 = models.IntegerField(default=0)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    attempted_by = models.CharField(max_length=1000, null=True)
    published_on = models.DateTimeField(auto_now=True)

    #Defination to generate the link of respective poll
    def get_poll_link(self):
        return ("http://127.0.0.1:8000/home/attempt/" + str(self.id))

    def __str__(self):
        return self.title
