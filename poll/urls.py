from django.urls import path
from . import views
urlpatterns = [
    path('',views.home,name='home'),
    path('login',views.login_user,name='login'),
    path('register',views.register,name='register'),
    path('logout',views.logout_user,name='logout'),
    path('createpoll',views.createpoll,name='createpoll'),
    path('mypolls',views.MyPolls,name='mypolls'),
    path('otherpolls',views.otherpolls,name='otherpolls'),
    path('getpoll',views.getpoll,name='getpoll'),
    path('attempt/<poll_id>',views.attemptpoll, name='attempt'),
    path('getresult/<poll_id>',views.getresults, name='getresult'),
    path('getlink/<poll_id>',views.getlink, name='getlink')
]