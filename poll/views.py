# imported required modules

from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import RegistrationForm, CreatePollForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import PollCreation


# views are here.

# View that renders the home page
def home(request):
    return render(request, 'home.html', {'title': 'home'})


# View that renders login Page
def login_user(request):
    if request.method == 'POST':
        e = request.POST.get('username')
        p = request.POST.get('password')
        user = authenticate(request, username=e, password=p)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.error(request, 'Wrong Username or Password')

    return render(request, 'login.html')


# View that renders register Page
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)
    else:
        form = RegistrationForm()
    context = {
        'form': form
    }
    return render(request, 'register.html', context)


# View that enables the feature of logout if user is logged in
def logout_user(request):
    logout(request)
    return redirect('home')


# view that helps to create the poll if the user is logged in
@login_required(login_url='login')
def createpoll(request):
    if request.method == 'POST':
        form = CreatePollForm(request.POST)
        if form.is_valid():
            poll = form.save(commit=False)
            poll.created_by = request.user
            poll.save()
            messages.success(request, "Successfully created the poll")
    else:
        form = CreatePollForm()
    context = {
        'form': form,
        'title': 'pollCreation'
    }
    return render(request, 'createpoll.html', context)


# view that renders all the polls created by the logged in user
@login_required(login_url='login')
def MyPolls(request):
    objs = PollCreation.objects.filter(created_by=request.user)
    context = {
        'mypolls': objs
    }
    return render(request, 'mypolls.html', context)


# view that renders all the polls created by the other users
@login_required(login_url='login')
def otherpolls(request):
    all = PollCreation.objects.filter(~Q(created_by=request.user))
    context = {
        'polls': all,
        'title': 'otherPolls'
    }
    return render(request, 'otherpolls.html', context)


# view that redirects the page that maps to the provided link
def getpoll(request):
    link = request.GET['link']
    return redirect(link)


# view that helps to attempt the poll if the user is logged in
@login_required(login_url='login')
def attemptpoll(request, poll_id):
    if request.method == 'POST':
        obj = PollCreation.objects.get(pk=poll_id)
        obj.attempted_by = request.user.username
        selected = request.POST['poll']
        if selected == obj.option1:
            obj.c1 = obj.c1 + 1
        elif selected == obj.option2:
            obj.c2 = obj.c2 + 1
        elif selected == obj.option3:
            obj.c3 = obj.c3 + 1
        elif selected == obj.option4:
            obj.c4 = obj.c4 + 1
        elif selected == obj.option5:
            obj.c5 = obj.c5 + 1
        obj.save()
    else:
        obj = PollCreation.objects.get(pk=poll_id)
    context = {
        'form': obj,
        'total': obj.c1 + obj.c2 + obj.c3 + obj.c4 + obj.c5,
        'title': 'attempt-poll'
    }
    return render(request, 'attemptpoll.html', context)


# view that renders the result of a selected poll
def getresults(request, poll_id):
    obj = PollCreation.objects.get(pk=poll_id)
    context = {
        'form': obj
    }
    return render(request, 'getresults.html', context)


# view that generates the link of a respective poll
def getlink(request, poll_id):
    obj = PollCreation.objects.get(pk=poll_id)
    context = {'link': obj.get_poll_link(),
               'title': 'Link'}
    print(context['link'])
    return render(request, 'link.html', context)
